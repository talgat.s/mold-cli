#!/usr/bin/env node

const path = require('path');
const {prompt} = require('enquirer');

const methods = require('../lib/');

const configPath = path.join(process.cwd(), '.moldrc.js');
const config = require(configPath);

(async function() {
	const commands = Object.keys(config);
	const {cmd} = await prompt([
		{
			type: 'autocomplete',
			name: 'cmd',
			message: 'Select command',
			choices: commands,
			suggest(input, choices) {
				return choices.filter(choice => choice.message.includes(input));
			}
		}
	]);

	const cmdConfig = config[cmd];
	const answers = await prompt(cmdConfig.questions);
	await cmdConfig.resolver(answers, methods);
})();
