const path = require('path');
const fs = require('fs');
const util = require('util');

const access = util.promisify(fs.access);
const mkdir = util.promisify(fs.mkdir);
const writeFile = util.promisify(fs.writeFile);

async function createDir(dir) {
	const dirs = dir.split(path.sep);

	let nextDir = '';
	for (const d of dirs) {
		try {
			nextDir = path.join(nextDir, d);
			await access(nextDir);
		} catch (e) {
			await mkdir(nextDir);
		}
	}
}

module.exports = async (resultPath, template) => {
	const {dir, base} = path.parse(resultPath);
	await createDir(dir);
	await writeFile(path.join(dir, base), template, 'utf8');
};
